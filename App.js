import React, { useEffect, useState } from 'react';
import { Button, Image, Text, TouchableOpacity, View } from 'react-native';
import styles from './styles';

const App = () => {
    const [isPlaying, setIsPlaying] = useState(false);
    const [playerCards, setPlayerCards] = useState({});
    const [aiCards, setAiCards] = useState({});
    const [aiTurn, setAiTurn] = useState(false);
    const [updater, setUpdater] = useState(0);
    const [currentScore, setCurrentScore] = useState(0);
    const [aiCurrentScore, setAiCurrentScore] = useState(0);
    const [roundEnd, setRoundEnd] = useState(false);

    const cards = {
        clubs: [
            2,3,4,5,6,7,8,9,10,'J','Q','K','A'
        ],
        spades: [
            2,3,4,5,6,7,8,9,10,'J','Q','K','A'
        ],
        hearts: [
            2,3,4,5,6,7,8,9,10,'J','Q','K','A'
        ],
        diamonds: [
            2,3,4,5,6,7,8,9,10,'J','Q','K','A'
        ],
    };

    const cardFiles = {
        clubs: {
            2: require('./assets/cards/2C.png'),
            3: require('./assets/cards/3C.png'),
            4: require('./assets/cards/4C.png'),
            5: require('./assets/cards/5C.png'),
            6: require('./assets/cards/6C.png'),
            7: require('./assets/cards/7C.png'),
            8: require('./assets/cards/8C.png'),
            9: require('./assets/cards/9C.png'),
            10: require('./assets/cards/10C.png'),
            'J': require('./assets/cards/JC.png'),
            'Q': require('./assets/cards/QC.png'),
            'K': require('./assets/cards/KC.png'),
            'A': require('./assets/cards/AC.png'),
        },
        spades: {
            2: require('./assets/cards/2S.png'),
            3: require('./assets/cards/3S.png'),
            4: require('./assets/cards/4S.png'),
            5: require('./assets/cards/5S.png'),
            6: require('./assets/cards/6S.png'),
            7: require('./assets/cards/7S.png'),
            8: require('./assets/cards/8S.png'),
            9: require('./assets/cards/9S.png'),
            10: require('./assets/cards/10S.png'),
            'J': require('./assets/cards/JS.png'),
            'Q': require('./assets/cards/QS.png'),
            'K': require('./assets/cards/KS.png'),
            'A': require('./assets/cards/AS.png'),
        },
        hearts: {
            2: require('./assets/cards/2H.png'),
            3: require('./assets/cards/3H.png'),
            4: require('./assets/cards/4H.png'),
            5: require('./assets/cards/5H.png'),
            6: require('./assets/cards/6H.png'),
            7: require('./assets/cards/7H.png'),
            8: require('./assets/cards/8H.png'),
            9: require('./assets/cards/9H.png'),
            10: require('./assets/cards/10H.png'),
            'J': require('./assets/cards/JH.png'),
            'Q': require('./assets/cards/QH.png'),
            'K': require('./assets/cards/KH.png'),
            'A': require('./assets/cards/AH.png'),
        },
        diamonds: {
            2: require('./assets/cards/2D.png'),
            3: require('./assets/cards/3D.png'),
            4: require('./assets/cards/4D.png'),
            5: require('./assets/cards/5D.png'),
            6: require('./assets/cards/6D.png'),
            7: require('./assets/cards/7D.png'),
            8: require('./assets/cards/8D.png'),
            9: require('./assets/cards/9D.png'),
            10: require('./assets/cards/10D.png'),
            'J': require('./assets/cards/JD.png'),
            'Q': require('./assets/cards/QD.png'),
            'K': require('./assets/cards/KD.png'),
            'A': require('./assets/cards/AD.png'),
        }
    }

    let usedCards = {};
    const maxScore = 21;
    const aiStayScore = 16;

    const startGame = () => {
        clearUsedCards();
        setAiTurn(false);
        setCurrentScore(0);
        setAiCurrentScore(0);
        setRoundEnd(false);

        if (isPlaying) {
            setIsPlaying(false);

            return;
        }

        setIsPlaying(true);
    };

    const clearUsedCards = () => {
        usedCards = {};
        setPlayerCards({});
        setAiCards({});
    };

    const addUsedCard = (category, cardIndex) => {
        if (category in usedCards) {
            usedCards[category].push(cards[category][cardIndex]);

            return;
        }

        usedCards[category] = [];
        usedCards[category].push(cards[category][cardIndex]);
    };

    const getRandomNumber = (maxInt) => {
        const randomNumber = Math.floor(Math.random() * Math.floor(maxInt));

        return randomNumber;
    }

    const checkUsedCard = (category, cardIndex) => {
        if (!(category in usedCards)) {
            addUsedCard(category, cardIndex);

            return true;
        }

        if (!usedCards[category].includes(cards[category][cardIndex])) {
            addUsedCard(category, cardIndex);

            return true;
        }

        return false;
    }

    const getUniqueCard = () => {
        let foundUniqueCard = false;
        let category = [];
        let cardIndex = -1;
        let counter = 0;

        while (!foundUniqueCard && counter < 52) {
            category = Object.keys(cards)[getRandomNumber(4)];
            cardIndex = getRandomNumber(13);

            if (checkUsedCard(category, cardIndex)) {
                foundUniqueCard = true;
            }

            counter += 1;
        }


        if (foundUniqueCard) {
            const card = {
                [category]: cards[category][cardIndex]
            };

            return card;
        }
    }

    const hitPlayer = () => {
        if (aiTurn || roundEnd) {
            return;
        }

        const card = getUniqueCard();

        if (typeof card === 'undefined') {
            return;
        }

        const cardCategory = Object.keys(card)[0];
        const cardName = card[cardCategory];

        let currentCards = playerCards;

        if (!(cardCategory in currentCards)) {
            currentCards[cardCategory] = [];
        }

        currentCards[cardCategory].push(cardName);

        let cardScore = cardName;
        if (typeof cardName === 'string') {
            cardScore = 10;

            if (cardName === 'A') {
                cardScore = ((currentScore + 11) > 21) ? 1 : 11;
            }
        }

        addScore('player', cardScore);
        setPlayerCards(currentCards);
        setUpdater(updater + 1);
    }

    const stayPlayer = () => {
        if (roundEnd) {
            return;
        }
        setAiTurn(true);
        aiRoundHandler();
    }

    const aiRoundHandler = () => {
        const card = getUniqueCard();

        if (typeof card === 'undefined') {
            return;
        }

        const cardCategory = Object.keys(card)[0];
        const cardName = card[cardCategory];

        let currentCards = aiCards;

        if (!(cardCategory in currentCards)) {
            currentCards[cardCategory] = [];
        }

        currentCards[cardCategory].push(cardName);

        let cardScore = cardName;
        if (typeof cardName === 'string') {
            cardScore = 10;

            if (cardName === 'A') {
                cardScore = ((aiCurrentScore + 11) > 21) ? 1 : 11;
            }
        }

        addScore('ai', cardScore);
        setAiCards(currentCards);
        setUpdater(updater + 1);
    }

    const addScore = (typeOfPlayer, scoreToAdd) => {
        if (typeOfPlayer === 'player') {
            setCurrentScore(currentScore + scoreToAdd);
        } else {
            setAiCurrentScore(aiCurrentScore + scoreToAdd);
        }
    };

    useEffect(() => {
        let newRound = false;
        if (currentScore === 21) {
            alert(`BLACKJACK, you win! . Your score = ${currentScore} . Ai score = ${aiCurrentScore}`);
            newRound = true;
        }

        if (aiCurrentScore === 21) {
            alert(`Ai scored a BLACKJACK, therefore you win!. Your score = ${currentScore} . Ai score = ${aiCurrentScore}`);
            newRound = true;
        }

        if (currentScore > 21) {
            alert(`You are busted, therefore you lose :( . Your score = ${currentScore} . Ai score = ${aiCurrentScore}`);
            newRound = true;
        }

        if (aiCurrentScore > 21) {
            alert(`Ai is busted, therefore you win!. Your score = ${currentScore} . Ai score = ${aiCurrentScore}`);
            newRound = true;
        }

        if (aiCurrentScore >= aiStayScore && !newRound) {
            if (aiCurrentScore >= currentScore) {
                alert(`Ai wins!, Your score = ${currentScore} . Ai score = ${aiCurrentScore}`);
                newRound = true;
            } else {
                alert(`You win!, Your score = ${currentScore} . Ai score = ${aiCurrentScore}`);
                newRound = true;
            }
        }

        if (aiCurrentScore < aiStayScore && aiTurn) {
            aiRoundHandler();
        }

        if (newRound) {
            setRoundEnd(true);
        }

        setUpdater(updater + 1);
    }, [currentScore, aiCurrentScore]);

    const view = () => {
        return (
            <View style={styles.main}>
                {
                    isPlaying &&
                    <View >
                        <Text style={styles.welcome__title}>Welcome to blackjack</Text>
                        <Text style={styles.welcome__subtitle}>Press the button to play!</Text>
                        <Button title='Press to Play' onPress={() => startGame()} />
                    </View>
                }
                {
                    !isPlaying &&
                    <View style={styles.game}>
                        <View style={styles.game__ai}>
                            <Text style={styles.gameAi__title}>Opponent cards:</Text>
                            <Text>
                                {
                                    Object.keys(aiCards).map((cardCategory, index) => {
                                        let cards = [];

                                        let cardCounter = 0;
                                        for (const card of aiCards[cardCategory]) {
                                            cards.push(<Image style={styles.card} source={cardFiles[cardCategory][card]} key={`ai_card-${cardCategory}-${cardCounter}`}></Image>);                                            cardCounter += 1;
                                        }

                                        return cards;
                                    })
                                }
                            </Text>
                        </View>
                        <View style={styles.game__player}>
                            <Text style={styles.gamePlayer__title}>Your cards:</Text>
                            <Text>
                                {
                                    Object.keys(playerCards).map((cardCategory, index) => {
                                        let cards = [];

                                        let cardCounter = 0;
                                        for (const card of playerCards[cardCategory]) {
                                            cards.push(<Image style={styles.card} source={cardFiles[cardCategory][card]} key={`player_card-${cardCategory}-${cardCounter}`}></Image>);
                                            cardCounter += 1;
                                        }

                                        return cards;
                                    })
                                }
                            </Text>
                        </View>
                        <View style={styles.game__controls}>
                                <TouchableOpacity style={[styles.button, styles.buttonGreen, (aiTurn || roundEnd) ? styles.isDisabled : null ]} onPress={() => hitPlayer()}><Text style={styles.buttonGreen}>Hit</Text></TouchableOpacity>
                                <TouchableOpacity style={[styles.button, styles.buttonRed, (roundEnd) ? styles.isDisabled : null]} onPress={() => stayPlayer()}><Text style={styles.buttonRed}>Stay</Text></TouchableOpacity>
                                <TouchableOpacity style={[styles.button, styles.buttonInfo]} onPress={() => startGame()}><Text style={styles.buttonInfo}>Quit</Text></TouchableOpacity>
                        </View>
                    </View>
                }
            </View>
        );
    }

    return view();
};

export default App;
