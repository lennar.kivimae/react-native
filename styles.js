import { StyleSheet} from 'react-native';

export default StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
        flexDirection: 'column'
    },
    button: {
        marginRight: 10,
        paddingHorizontal: 22,
        paddingVertical: 8,
        fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif'
    },
    buttonGreen: {
        color: 'white',
        backgroundColor: '#28a745'
    },
    buttonRed: {
        color: 'white',
        backgroundColor: '#dc3545'
    },
    buttonInfo: {
        color: 'white',
        backgroundColor: '#17a2b8'
    },
    welcome: {
        flexDirection: 'column',
    },
    welcome__subtitle: {
        alignSelf: 'center',
        marginBottom: 30
    },
    welcome__title: {
        fontSize: 30,
        marginBottom: 10
    },
    game: {
        height: '100%',
        maxWidth: '80%',
        justifyContent: 'space-evenly',
        paddingVertical: 50,
    },
    game__text: {
        display: 'flex',
        alignSelf: 'center'
    },
    game__controls: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    gameAi__title: {
        fontSize: 28,
        fontWeight: '500',
        marginBottom: 12
    },
    gamePlayer__title: {
        fontSize: 28,
        fontWeight: '500',
        marginBottom: 12
    },
    isDisabled: {
        opacity: .2,
    },
    card: {
        height: 80,
        width: 60
    }
});
